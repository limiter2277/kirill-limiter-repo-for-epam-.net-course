﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    /// <summary>
    /// Класс, реализующий работу с матрицей, которую он в себе и хранит.
    /// </summary>
    public class Matrix
    {
        /// <summary>
        /// Прямоугольная матрица.
        /// </summary>
        public double[,] M { get; set; }

        public Matrix(double[,] m)
        {
            M = m;
        }

        /// <summary>
        /// Проверяет, является ли матрица совместимой с другой матрицей.
        /// </summary>
        /// <param name="m">Матрица, с которой проверяется совместимость.</param>
        /// <returns>Результат проверки.</returns>
        public bool Compatible(Matrix m)
        {
            return M.GetLength(1) == m.M.GetLength(0);
        }

        /// <summary>
        /// Умножает матрицу на другую матрицу, если они совместимы.
        /// </summary>
        /// <param name="m">Матрица, на которую будет произведено умножение.</param>
        /// <returns>Результирующая матрица.</returns>
        public Matrix Multiply(Matrix m)
        {
            if (!Compatible(m))
            {
                throw new Exception("Матрицы нельзя перемножить.");
            }

            int ma = M.GetLength(0);
            int mb = m.M.GetLength(0);
            int nb = m.M.GetLength(1);

            double[,] r = new double[ma, nb];

            for (int i = 0; i < M.GetLength(0); i++)
            {
                for (int j = 0; j < nb; j++)
                {
                    for (int k = 0; k < mb; k++)
                    {
                        r[i, j] += M[i, k] * m.M[k, j];
                    }
                }
            }
            return new Matrix(r);
        }

        public override string ToString()
        {
            var str = new StringBuilder();
            for (int i = 0; i < M.GetLength(0); i++)
            {
                for (int j = 0; j < M.GetLength(1); j++)
                {
                    str.Append(M[i, j]);
                    str.Append(" ");
                }
                str.AppendLine();
            }
            return str.ToString();
        }

        public override bool Equals(object other) // Для юнит-теста.
        {
            var toCompareWith = other as Matrix;
            if (toCompareWith == null)
                return false;
            if (M.GetLength(0) != toCompareWith.M.GetLength(0) || M.GetLength(1) != toCompareWith.M.GetLength(1))
            {
                return false;
            }

            for (int i = 0; i < M.GetLength(0); i++)
            {
                for (int j = 0; j < M.GetLength(1); j++)
                {
                    if (M[i,j] != toCompareWith.M[i, j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override int GetHashCode() // Стандартная реализация от студии. Оставим пока так :)
        {
            return -2048098088 + EqualityComparer<double[,]>.Default.GetHashCode(M);
        }
    }
}
