﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Task5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LogTextBox.Text += "Ввод матриц происходит в правой половине окна по строкам через пробел. Пример:" + Environment.NewLine + "12 45" + Environment.NewLine + "98 3" + Environment.NewLine;
        }

        private double[,] ParseMatrix(string str)
        {
            if (str == String.Empty)
            {
                throw new ArgumentException("Необходимо ввести матрицу.");
            }

            var lines = new List<List<double>>();

            foreach (var line in str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
            {
                lines.Add(new List<double>());
                foreach (var number in line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (!Double.TryParse(number, NumberStyles.Any, CultureInfo.InvariantCulture, out double x))
                    {
                        throw new FormatException("The following data cannot be parsed from string to double: " + number);
                    }
                    lines[lines.Count - 1].Add(x);
                }
            }

            double[,] result = new double[lines.Count, lines[0].Count];
            for (int i = 0; i < lines.Count; i++)
            {
                if (lines[0].Count != lines[i].Count)
                {
                    throw new ArgumentException("Введенная матрица не является прямоугольной.");
                }
                for (int j = 0; j < lines[i].Count; j++)
                {
                    result[i, j] = lines[i][j];
                }
            }
            return result;
        }

        private void CheckButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var m1 = new Matrix(ParseMatrix(Matrix1TextBox.Text));
                var m2 = new Matrix(ParseMatrix(Matrix2TextBox.Text));
                LogTextBox.Text += (m1.Compatible(m2) ? "Матрицы совместимы." : "Матрицы не совместимы.") + Environment.NewLine;
            }
            catch (Exception ex)
            {
                LogTextBox.Text += ex.Message + Environment.NewLine;
            }
            LogTextBox.ScrollToEnd();
        }

        private void MultiplyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var m1 = new Matrix(ParseMatrix(Matrix1TextBox.Text));
                var m2 = new Matrix(ParseMatrix(Matrix2TextBox.Text));
                var m3 = m1.Multiply(m2);
                LogTextBox.Text += "Результат умножения:" + Environment.NewLine + m3;
            }
            catch (Exception ex)
            {
                LogTextBox.Text += ex.Message + Environment.NewLine;
            }
        }
    }
}
