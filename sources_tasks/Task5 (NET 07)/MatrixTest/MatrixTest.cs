﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task5;

namespace MatrixTest
{
    [TestClass]
    public class MatrixTest
    {
        [TestMethod]
        public void MatrixCompatible_CompatibleMatrixes_True()
        {
            // Arrange
            var m1 = new Matrix(new double[3, 2]
                {
                    { 1, 1 },
                    { 1, 1 },
                    { 1, 1 }
                });
            var m2 = new Matrix(new double[2, 3]
                {
                    { 2, 2, 2 },
                    { 2, 2, 2 }
                });
            bool expected = true;

            // Act
            var actual = m1.Compatible(m2);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MatrixCompatible_IncompatibleMatrixes_False()
        {
            // Arrange
            var m1 = new Matrix(new double[3, 2]
                {
                    { 1, 1 },
                    { 1, 1 },
                    { 1, 1 }
                });
            var m2 = new Matrix(new double[3, 3]
                {
                    { 2, 2, 2 },
                    { 2, 2, 2 },
                    { 2, 2, 2 }
                });
            bool expected = false;

            // Act
            var actual = m1.Compatible(m2);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MatrixMultiply_CompatibleMatrixes_ResultMatrix()
        {
            // Arrange
            var m1 = new Matrix(new double[2, 2]
                {
                    { 1, 1 },
                    { 1, 1 }
                });
            var m2 = new Matrix(new double[2, 2]
                {
                    { 2, 2 },
                    { 2, 2 }
                });
            var expected = new Matrix(new double[2, 2]
                {
                    { 4, 4 },
                    { 4, 4 }
                });

            // Act
            var actual = m1.Multiply(m2);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
