﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4_Decorators
{
    class Program
    {
        static void Main(string[] args)
        {
            Stream fs = new FileStream(Path.GetFileName("test.txt"), FileMode.Create); // Пересоздаем файл всегда.
            fs = new StreamReadWithPasswordDecorator(fs); // Декорируем чтением с паролем.
            fs = new StreamPositionPercentageDecorator(fs); // Декорируем выводом позиции в файле в процентах.

            try
            {
                using (StreamWriter sr = new StreamWriter(fs))
                {
                    for (int i = 0; i < 2000; i++)
                    {
                        sr.Write(i); // Заполняем числами. Много чисел, т.к. чтение происходит порциями. На маленьком объеме будет всего одно чтение файла - так не интересно.
                    }
                }
                fs.Position = 0; // Возвращаемся в начало стрима.
                using (StreamReader sr = new StreamReader(fs))
                {
                    while (!sr.EndOfStream)
                    {
                        Console.Write(Convert.ToChar(sr.Read()) + " "); // Читаем символы по одному.
                        var x = fs.Position; // Запрашиваем позицию для триггера функционала декоратора.
                        // Процент позиции в стриме одинаков при каждом выводе и обновляется после ввода пароля на чтение (т.е., собственно, при чтении).
                        // Это происходит из-за чтения порциями по 1024 байт, т.е. Position не меняется при выводе, пока не понадобится прочитать следующий кусок файла.
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
