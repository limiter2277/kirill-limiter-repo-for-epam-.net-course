﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4_Decorators
{
    class StreamReadWithPasswordDecorator : StreamBaseDecorator
    {
        private const string PASSWORD = "123";

        public StreamReadWithPasswordDecorator(Stream wr) : base(wr) { }

        /// <summary>
        /// Выполняет чтение из потока с предварительным запросом пароля от пользователя.
        /// </summary>
        /// <param name="buffer">Буфер для чтения.</param>
        /// <param name="offset">Сдвиг при записи в буфер.</param>
        /// <param name="count">Кол-во байт для чтения.</param>
        /// <returns>Сдвиг позиции в потоке stream в результате чтения.</returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            Console.WriteLine();
            Console.Write($"Please enter your password to continue reading (type {PASSWORD}): ");
            string pw = Console.ReadLine();
            if (pw == PASSWORD)
            {
                return wrapped.Read(buffer, offset, count);
            }
            else
            {
                Console.WriteLine("Wrong password.");
                return 0;
                // По задумке, он просто должен был якобы прочитать, но без результатов (без сдвига позиции),
                // однако, судя по всему, сразу триггерится EndOfStream, и происходит выход из программы. Ну и ладно.
            }
        }
    }
}
