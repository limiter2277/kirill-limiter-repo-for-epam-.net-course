﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4_Decorators
{
    class StreamPositionPercentageDecorator : StreamBaseDecorator
    {
        public StreamPositionPercentageDecorator(Stream wr) : base(wr) { }

        /// <summary>
        /// В дополнение к возврату позиции в потоке stream выводит на экран прогресс работы с файлом - отношение текущей позиции к длине файла.
        /// </summary>
        public override long Position{
            get
            {
                Console.WriteLine("Position percentage: " + (wrapped.Length == 0 ? 0 : (double)(wrapped.Position) / wrapped.Length).ToString("0.000"));
                return wrapped.Position;
            }
        }
    }
}
