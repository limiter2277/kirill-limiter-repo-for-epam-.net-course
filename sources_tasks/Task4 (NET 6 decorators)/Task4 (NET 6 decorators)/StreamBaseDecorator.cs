﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Task4_Decorators
{
    class StreamBaseDecorator : Stream
    {
        protected Stream wrapped; // Упакованный (декорированный) объект.

        // Далее ничего интересного, просто реализации абстрактов из Stream. По хорошему, нужен интерфейс вместо наследования.

        public override bool CanRead => wrapped.CanRead;

        public override bool CanSeek => wrapped.CanSeek;

        public override bool CanWrite => wrapped.CanWrite;

        public override long Length => wrapped.Length;

        public override long Position { get => wrapped.Position; set => wrapped.Position = value; }

        public override void Flush()
        {
            wrapped.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return wrapped.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return wrapped.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            wrapped.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            wrapped.Write(buffer, offset, count);
        }

        public StreamBaseDecorator(Stream wr)
        {
            wrapped = wr;
        }
    }
}
