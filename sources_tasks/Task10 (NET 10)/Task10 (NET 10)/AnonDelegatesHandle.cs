﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class AnonDelegatesHandle : DefaultHandle, ICutDownNotifier
    {
        public AnonDelegatesHandle(TaskTimerStarted taskTimerStarted, Action<string, int> taskTimerEnded) : base(taskTimerStarted, taskTimerEnded) { }

        public override void Init(TimerImitation timer)
        {
            _timer = timer ?? throw new ArgumentNullException(nameof(timer));
            _timerDuration = timer.SecondsRemaining;

            timer.TimerStartsEvent += delegate(object sender, EventArgs e) { _handleTimerStartsEvent(_timer.Name, _timerDuration); };
            timer.TimerEndsEvent += delegate(object sender, EventArgs e) { _handleTimerEndsEvent(_timer.Name, _timerDuration); };
            timer.TimerTickEvent += delegate(object sender, TimerTickEventArgs e) { Console.WriteLine($"ANON DELEGATE: Timer {(_timer.Name == String.Empty ? "(no name)" : _timer.Name)} has {e.SecondsRemaining} sec. remaining."); };
        }
    }
}
