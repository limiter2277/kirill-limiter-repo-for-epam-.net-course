﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    delegate void TaskTimerStarted(string taskName, int taskTime); // Доступен из namespace напрямую.

    class Program
    {
        static void TaskStarts(string taskName, int taskTime)
        {
            Console.WriteLine($"{(taskName == String.Empty ? "(no timer name)" : taskName)} началось с {taskTime} сек. отведенного времени.");
        }

        static void TaskEnds(string taskName, int taskTime)
        {
            Console.WriteLine($"{(taskName == String.Empty ? "(no timer name)" : taskName)} закончилось с {taskTime} сек. отведенного времени.");
        }

        static void Main(string[] args)
        {
            /*
            var timer = new TimerImitation(5, "SuperTimer");
            timer.TimerStartsEvent += (sender, arg) => Console.WriteLine($"Timer {(timer.Name == String.Empty ? "(no name)" : timer.Name)} starts");
            timer.TimerEndsEvent += (sender, arg) => Console.WriteLine($"Timer {(timer.Name == String.Empty ? "(no name)" : timer.Name)} ends");
            timer.TimerTickEvent += (sender, arg) => Console.WriteLine($"Timer {(timer.Name == String.Empty ? "(no name)" : timer.Name)} has {arg.SecondsRemaining} sec. remaining.");
            timer.Run();
            */

            var handlers = new List<ICutDownNotifier>
            {
                new MethodHandle(TaskStarts, TaskEnds),
                new AnonDelegatesHandle(TaskStarts, TaskEnds),
                new LambdaHandle(TaskStarts, TaskEnds)
            };

            handlers[0].Init(new TimerImitation(3, "Чтение задания"));
            handlers[1].Init(new TimerImitation(3, "Выполнение задания"));
            handlers[2].Init(new TimerImitation(3, "Проверка задания перед отправкой"));

            try
            {
                foreach (var handler in handlers)
                {
                    handler.Run();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.ReadKey();
        }
    }
}
