﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class TimerTickEventArgs : EventArgs
    {
        public int SecondsRemaining { get; set; }

        public TimerTickEventArgs(int seconds)
        {
            SecondsRemaining = seconds;
        }
    }
}
