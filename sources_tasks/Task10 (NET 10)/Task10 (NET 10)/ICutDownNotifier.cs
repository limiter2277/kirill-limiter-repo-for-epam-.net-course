﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    interface ICutDownNotifier
    {
        /// <summary>
        /// Инициализирует обработчик указанным таймером и подписывается на его события.
        /// </summary>
        /// <param name="t">Используемый таймер.</param>
        void Init(TimerImitation t);

        /// <summary>
        /// Запуск используемого обработчиком таймера.
        /// </summary>
        void Run();
    }
}
