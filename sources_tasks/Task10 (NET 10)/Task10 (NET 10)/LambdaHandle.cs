﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class LambdaHandle : DefaultHandle, ICutDownNotifier
    {
        public LambdaHandle(TaskTimerStarted taskTimerStarted, Action<string, int> taskTimerEnded) : base(taskTimerStarted, taskTimerEnded) { }

        public override void Init(TimerImitation timer)
        {
            _timer = timer ?? throw new ArgumentNullException(nameof(timer));
            _timerDuration = timer.SecondsRemaining;

            timer.TimerStartsEvent += (object sender, EventArgs e) => { _handleTimerStartsEvent(_timer.Name, _timerDuration); };
            timer.TimerEndsEvent += (object sender, EventArgs e) => { _handleTimerEndsEvent(_timer.Name, _timerDuration); };
            timer.TimerTickEvent += (object sender, TimerTickEventArgs e) => { Console.WriteLine($"LAMBDA: Timer {(_timer.Name == String.Empty ? "(no name)" : _timer.Name)} has {e.SecondsRemaining} sec. remaining."); };
        }
    }
}
