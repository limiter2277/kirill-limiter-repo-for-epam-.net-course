﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Task10
{
    class TimerImitation
    {
        public int SecondsRemaining { get; protected set; }
        public string Name { get; protected set; }

        public TimerImitation(int duration, string name)
        {
            SecondsRemaining = duration;
            Name = name;
        }
        public TimerImitation(int duration) : this(duration, String.Empty) { }

        public event EventHandler<EventArgs> TimerStartsEvent;
        public event EventHandler<EventArgs> TimerEndsEvent;
        public event EventHandler<TimerTickEventArgs> TimerTickEvent;

        public void Run()
        {
            OnTimerStartsEvent(new EventArgs());

            while (SecondsRemaining >= 0)
            {
                if (SecondsRemaining == 0)
                {
                    OnTimerEndsEvent(new EventArgs());
                    break;
                }

                OnTimerTickEvent(new TimerTickEventArgs(SecondsRemaining));
                SecondsRemaining--;
                Thread.Sleep(1000);
            }
        }

        protected virtual void OnTimerStartsEvent(EventArgs e)
        {
            EventHandler<EventArgs> handler = TimerStartsEvent;
            handler?.Invoke(this, e);
        }

        protected virtual void OnTimerEndsEvent(EventArgs e)
        {
            EventHandler<EventArgs> handler = TimerEndsEvent;
            handler?.Invoke(this, e);
        }

        protected virtual void OnTimerTickEvent(TimerTickEventArgs e)
        {
            EventHandler<TimerTickEventArgs> handler = TimerTickEvent;
            handler?.Invoke(this, e);
        }
    }
}
