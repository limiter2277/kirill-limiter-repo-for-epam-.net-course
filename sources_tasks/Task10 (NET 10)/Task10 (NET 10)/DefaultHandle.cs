﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    abstract class DefaultHandle
    {
        protected TimerImitation _timer;
        protected int _timerDuration;
        protected TaskTimerStarted _handleTimerStartsEvent;
        protected Action<string, int> _handleTimerEndsEvent;

        public DefaultHandle(TaskTimerStarted taskTimerStarted, Action<string, int> taskTimerEnded)
        {
            _handleTimerStartsEvent = taskTimerStarted;
            _handleTimerEndsEvent = taskTimerEnded;
        }

        public abstract void Init(TimerImitation timer);

        public void Run()
        {
            if (_timer == null)
            {
                throw new InvalidOperationException("Cannot run a timer because it's null. Do the initialization first.");
            }

            _timer.Run();
        }
    }
}
