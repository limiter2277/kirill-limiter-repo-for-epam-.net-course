﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class MethodHandle : DefaultHandle, ICutDownNotifier
    {
        public MethodHandle(TaskTimerStarted taskTimerStarted, Action<string, int> taskTimerEnded) : base(taskTimerStarted, taskTimerEnded) { }

        public override void Init(TimerImitation timer)
        {
            _timer = timer ?? throw new ArgumentNullException(nameof(timer));
            _timerDuration = timer.SecondsRemaining;

            timer.TimerStartsEvent += HandleTimerStartsEvent;
            timer.TimerEndsEvent += HandleTimerEndsEvent;
            timer.TimerTickEvent += HandleTimerTickEvent;
        }

        void HandleTimerStartsEvent(object sender, EventArgs e)
        {
            //Console.WriteLine($"METHOD: Timer {(_timer.Name == String.Empty ? "(no name)" : _timer.Name)} starts");
            _handleTimerStartsEvent(_timer.Name, _timerDuration);
        }

        void HandleTimerEndsEvent(object sender, EventArgs e)
        {
            //Console.WriteLine($"METHOD: Timer {(_timer.Name == String.Empty ? "(no name)" : _timer.Name)} ends");
            _handleTimerEndsEvent(_timer.Name, _timerDuration);
        }

        void HandleTimerTickEvent(object sender, TimerTickEventArgs e)
        {
            Console.WriteLine($"METHOD: Timer {(_timer.Name == String.Empty ? "(no name)" : _timer.Name)} has {e.SecondsRemaining} sec. remaining.");
        }
    }
}
