﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    /// <summary>
    /// Компаратор для класса Int32.
    /// </summary>
    class IntReverseComparer : Comparer<Int32>
    {
        public override int Compare(Int32 x, Int32 y)
        {
            if (object.Equals(x, y))
            {
                return 0;
            }
            return (x.CompareTo(y) * -1);
        }
    }
}
