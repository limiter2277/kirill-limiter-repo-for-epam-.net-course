﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    /// <summary>
    /// Класс, реализующий бинарное дерево поиска.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class BinarySearchTree<T> : IEnumerable<T> where T : IComparable<T>
    {
        protected Node<T> root;
        protected IComparer<T> comparer;

        /// <summary>
        /// Конструктор со стандартным компарером.
        /// </summary>
        public BinarySearchTree() : this(Comparer<T>.Default) { }

        /// <summary>
        /// Конструктор с явно указанным компарером.
        /// </summary>
        /// <param name="defaultComparer">Компарер для элементов дерева.</param>
        public BinarySearchTree(IComparer<T> defaultComparer)
        {
            comparer = defaultComparer ?? throw new ArgumentNullException("Default comparer is null");
        }

        /// <summary>
        /// Конструктор с коллекцией элементов для инициализации дерева.
        /// </summary>
        /// <param name="collection">Коллекция элементов для инициализации дерева.</param>
        public BinarySearchTree(IEnumerable<T> collection) : this(collection, Comparer<T>.Default) { }
        public BinarySearchTree(IEnumerable<T> collection, IComparer<T> defaultComparer) : this(defaultComparer)
        {
            AddRange(collection);
        }

        /// <summary>
        /// Перебор элементов в порядке возрастания.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> InOrder()
        {
            if (root == null)
            {
                yield break;
            }

            var stack = new Stack<Node<T>>();
            var node = root;

            // Топаем по дереву, начиная с левых веток.
            while (stack.Count > 0 || node != null)
            {
                if (node == null)
                {
                    node = stack.Pop();
                    yield return node.Value;
                    node = node.Right;
                }
                else
                {
                    stack.Push(node);
                    node = node.Left;
                }
            }
        }

        /// <summary>
        /// Перебор элементов в порядке убывания.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> PostOrder()
        {
            if (root == null)
            {
                yield break;
            }

            var stack = new Stack<Node<T>>();
            var node = root;

            // Топаем по дереву, начиная с левых веток.
            while (stack.Count > 0 || node != null)
            {
                if (node == null)
                {
                    node = stack.Pop();
                    yield return node.Value;
                    node = node.Left;
                }
                else
                {
                    stack.Push(node);
                    node = node.Right;
                }
            }
        }

        /// <summary>
        /// Перебор элементов в порядке хранения в дереве.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> TreeOrder()
        {
            if (root == null)
            {
                yield break;
            }

            var stack = new Stack<Node<T>>();
            stack.Push(root);

            // Топаем по дереву сверху вниз слева направо.
            while (stack.Count > 0)
            {
                var node = stack.Pop();
                yield return node.Value;
                if (node.Right != null)
                {
                    stack.Push(node.Right);
                }
                if (node.Left != null)
                {
                    stack.Push(node.Left);
                }
            }
        }

        /// <summary>
        /// Кол-во элементов в дереве.
        /// </summary>
        public int Count
        {
            get;
            protected set;
        }

        /// <summary>
        /// Добавляет элемент в дерево.
        /// </summary>
        /// <param name="item">Элемент для добавления.</param>
        public void Add(T item)
        {
            var node = new Node<T>(item);

            if (root == null)
            {
                root = node;
            }
            else
            {
                Node<T> current = root, parent = null;

                // Ищем место.
                while (current != null)
                {
                    parent = current;
                    if (comparer.Compare(item, current.Value) < 0)
                    {
                        current = current.Left;
                    }
                    else
                    {
                        current = current.Right;
                    }
                }

                // Пихаем новый элемент.
                if (comparer.Compare(item, parent.Value) < 0)
                {
                    parent.Left = node;
                }
                else
                {
                    parent.Right = node;
                }
            }

            Count++;
        }

        /// <summary>
        /// Добавляет несколько элементов в дерево.
        /// </summary>
        /// <param name="collection">Коллекция элементов для добавления.</param>
        public void AddRange(IEnumerable<T> collection)
        {
            foreach (var value in collection)
            {
                Add(value);
            }
        }

        /// <summary>
        /// Удаляет элемент из дерева.
        /// </summary>
        /// <param name="item">Элемент для удаления.</param>
        public void Remove(T item)
        {
            if (root == null)
            {
                return;
            }

            Node<T> current = root, parent = null;

            int result;

            // Ищем элемент.
            do
            {
                result = comparer.Compare(item, current.Value);
                if (result < 0)
                {
                    parent = current;
                    current = current.Left;
                }
                else if (result > 0)
                {
                    parent = current;
                    current = current.Right;
                }
                if (current == null)
                {
                    return;
                }
            }
            while (result != 0);

            // Удаляем.
            if (current.Right == null)
            {
                if (current == root)
                {
                    root = current.Left;
                }
                else
                {
                    result = comparer.Compare(current.Value, parent.Value);
                    if (result < 0)
                    {
                        parent.Left = current.Left;
                    }
                    else
                    {
                        parent.Right = current.Left;
                    }
                }
            }
            else if (current.Right.Left == null)
            {
                current.Right.Left = current.Left;
                if (current == root)
                {
                    root = current.Right;
                }
                else
                {
                    result = comparer.Compare(current.Value, parent.Value);
                    if (result < 0)
                    {
                        parent.Left = current.Right;
                    }
                    else
                    {
                        parent.Right = current.Right;
                    }
                }
            }
            else
            {
                Node<T> min = current.Right.Left, prev = current.Right;
                while (min.Left != null)
                {
                    prev = min;
                    min = min.Left;
                }
                prev.Left = min.Right;
                min.Left = current.Left;
                min.Right = current.Right;

                if (current == root)
                {
                    root = min;
                }
                else
                {
                    result = comparer.Compare(current.Value, parent.Value);
                    if (result < 0)
                    {
                        parent.Left = min;
                    }
                    else
                    {
                        parent.Right = min;
                    }
                }
            }

            Count--;
        }

        /// <summary>
        /// Удаляет из дерева все элементы.
        /// </summary>
        public void Clear()
        {
            root = null;
            Count = 0;
        }

        /// <summary>
        /// Проверяет, содержит ли дерева указанный элемент.
        /// </summary>
        /// <param name="item">Элемент для проверки.</param>
        /// <returns></returns>
        public bool Contains(T item)
        {
            var current = root;
            while (current != null)
            {
                var result = comparer.Compare(item, current.Value);
                if (result == 0)
                {
                    return true;
                }
                if (result < 0)
                {
                    current = current.Left;
                }
                else
                {
                    current = current.Right;
                }
            }
            return false;
        }

        /// <summary>
        /// Возвращает перечислитель в порядке возрастания.
        /// </summary>
        /// <returns>Перечислитель в порядке возрастания</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return InOrder().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Возвращает перечислитель в порядке убывания.
        /// </summary>
        /// <returns>Перечислитель в порядке убывания</returns>
        public IEnumerator<T> GetReversedEnumerator()
        {
            return PostOrder().GetEnumerator();
        }
    }
}
