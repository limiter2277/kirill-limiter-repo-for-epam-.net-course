﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    /// <summary>
    /// Компаратор для класса TestResults.
    /// </summary>
    class TestResultsComparer : Comparer<TestResults>
    {
        public override int Compare(TestResults x, TestResults y)
        {
            if (object.Equals(x, y))
            {
                return 0;
            }
            return (x.ResultMark.CompareTo(y.ResultMark) * -1);
        }
    }
}
