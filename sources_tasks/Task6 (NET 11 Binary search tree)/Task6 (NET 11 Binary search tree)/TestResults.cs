﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    /// <summary>
    /// Класс, представляющий результаты теста студента.
    /// </summary>
    class TestResults : IComparable<TestResults>
    {
        /// <summary>
        /// Имя студента.
        /// </summary>
        public string StudentName { get; protected set; }

        /// <summary>
        /// Название теста.
        /// </summary>
        public string TestName { get; protected set; }

        /// <summary>
        /// Дата проведения теста.
        /// </summary>
        public DateTime Date { get; protected set; }

        /// <summary>
        /// Оценка студента за тест.
        /// </summary>
        public int ResultMark { get; protected set; }

        /// <summary>
        /// Конструктор со всеми свойствами.
        /// </summary>
        /// <param name="studentName">Имя студента.</param>
        /// <param name="testName">Название теста.</param>
        /// <param name="date">Дата проведения теста.</param>
        /// <param name="resultMark">Оценка студента за тест.</param>
        public TestResults(string studentName, string testName, DateTime date, int resultMark)
        {
            StudentName = studentName;
            TestName = testName;
            Date = date;
            ResultMark = resultMark;
        }

        public int CompareTo(TestResults other)
        {
            return new TestResultsComparer().Compare(this, other);
        }

        public override string ToString()
        {
            return $"{StudentName} выполнил тест \"{TestName}\" {Date.ToShortDateString()} и получил оценку {ResultMark}.";
        }
    }
}
