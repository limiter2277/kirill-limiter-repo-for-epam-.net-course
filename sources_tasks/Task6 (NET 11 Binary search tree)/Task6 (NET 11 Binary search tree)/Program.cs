﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            var intTree = new BinarySearchTree<int>(new int[] { 11, 25, 73, 64, 5 }, new IntReverseComparer());
            intTree.AddRange(new int[] { 61, 17, 8 });

            Console.WriteLine("Вывод дерева с числами в порядке нормального сравнения (убывание, см. компаратор):");
            foreach (var item in intTree)
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();

            Console.WriteLine("Вывод дерева с числами в порядке обратного сравнения:");
            foreach (var item in intTree.PostOrder())
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();

            Console.WriteLine("Вывод дерева с числами в порядке хранения в дереве:");
            foreach (var item in intTree.TreeOrder())
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();
            Console.WriteLine();



            // Компаратор можно не указывать, т.к. он вызывается в переопределенном CompareTo в самом классе TestResults.
            var testResultsTree = new BinarySearchTree<TestResults>(new TestResults[] {
                new TestResults("Геннадий Букин", "Знание футбола", new DateTime(2018, 6, 16), 5),
                new TestResults("Андрей Кулебякин", "Корейский язык", new DateTime(2018, 5, 6), 3),
                new TestResults("Леонид Воронин", "Кулинарные рецепты", new DateTime(2018, 2, 9), 4),
                new TestResults("Мария Леонова", "Юриспруденция", new DateTime(2018, 3, 27), 3),
                new TestResults("Александра Левина", "Основы C++", new DateTime(2018, 1, 1), 4),
                new TestResults("Дмитрий Прозоров", "Компьютерная графика", new DateTime(2018, 2, 16), 5),
                new TestResults("Евгений Яфаркин", "Ролеплей: игра в заказчика", new DateTime(2018, 7, 10), 999),
                });

            Console.WriteLine("Вывод дерева с тестами в порядке нормального сравнения (убывание, см. компаратор):");
            foreach (var item in testResultsTree)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            Console.WriteLine("Вывод дерева с тестами в порядке хранения в дереве:");
            foreach (var item in testResultsTree.TreeOrder())
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();





            var stringTree = new BinarySearchTree<string>(new string[] { "one", "two", "three", "four", "five", "six", "seven" });

            Console.WriteLine("Вывод дерева со строками в порядке обратного сравнения:");
            foreach (var item in stringTree.PostOrder())
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
