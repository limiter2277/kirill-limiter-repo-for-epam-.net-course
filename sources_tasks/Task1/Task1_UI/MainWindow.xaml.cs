﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Task1_Utility;

namespace Task1_UI
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<string> lastFormatResult = new List<string>();

        public MainWindow()
        {
            InitializeComponent();
            LogTextBox.Text += "Hello World! Программа запущена." + Environment.NewLine;
        }

        private void InsertExampleDataButton_Click(object sender, RoutedEventArgs e)
        {
            DataTextBox.Text = String.Empty;
            foreach (string str in Utility.EXAMPLE_DATA)
            {
                DataTextBox.Text += str + Environment.NewLine;
            }
            LogTextBox.Text += "Пример входных данных помещен в соответствующее поле." + Environment.NewLine;
        }

        private void FormatDataButton_Click(object sender, RoutedEventArgs e)
        {
            LogTextBox.Text += "Форматируем введенные данные..." + Environment.NewLine;
            lastFormatResult = new List<string>();
            foreach (string str in DataTextBox.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
            {
                string resultStr = String.Empty;
                try
                {
                    resultStr = Utility.Format(str);
                    LogTextBox.Text += resultStr + Environment.NewLine;
                    lastFormatResult.Add(resultStr);
                }
                catch (FormatException ex)
                {
                    LogTextBox.Text += ex.Message + Environment.NewLine;
                }
            }
        }

        private void ReadFileButton_Click(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            try
            {
                data = Utility.ReadFile(SourceFileTextBox.Text);
            }
            catch (Exception ex)
            {
                LogTextBox.Text += "Не удалось прочитать файл: " + ex.Message + Environment.NewLine;
                return;
            }
            DataTextBox.Text = String.Empty;
            foreach (string str in data)
            {
                DataTextBox.Text += str + Environment.NewLine;
            }
            LogTextBox.Text += "Входные данные из файла помещены в соответствующее поле." + Environment.NewLine;
        }

        private void WriteFileButton_Click(object sender, RoutedEventArgs e)
        {
            if (lastFormatResult.Count == 0)
            {
                LogTextBox.Text += "Нет данных для записи. Форматирование еще не выполнялось или последнее было неудачным?" + Environment.NewLine;
            }
            else
            {
                try
                {
                    Utility.WriteFile(TargetFileTextBox.Text, lastFormatResult);
                }
                catch (Exception ex)
                {
                    LogTextBox.Text += "Не удалось записать файл: " + ex.Message + Environment.NewLine;
                    return;
                }
                LogTextBox.Text += "Результат последнего форматирования записан в файл." + Environment.NewLine;
            }
        }
    }
}
