﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task1_Utility;

namespace Task1_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            // Выбираем метод работы исходя из кол-ва аргументов.
            switch (args.Length)
            {
                // Аргументов нет, используем данные из примера.
                case 0:
                    Console.WriteLine("Example data:");
                    foreach (string str in Utility.EXAMPLE_DATA)
                    {
                        Console.WriteLine(str);
                    }

                    Console.WriteLine(Environment.NewLine + "Result data:");
                    foreach (string str in Utility.EXAMPLE_DATA)
                    {
                        string resultStr = String.Empty;
                        try
                        {
                            resultStr = Utility.Format(str);
                            Console.WriteLine(resultStr);
                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    Console.WriteLine(Environment.NewLine + "Press any key...");
                    Console.ReadKey();
                    break;

                // Есть 1 или 2 аргумента (исходный_файл[, целевой_файл])
                case 1:
                case 2:
                    string sourceFileName = args[0];
                    //string targetFileName = args[1];
                    List<string> data = new List<string>();

                    // Читаем исходный файл.
                    try
                    {
                        data = Utility.ReadFile(sourceFileName);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("The file could not be read:");
                        Console.WriteLine(e.Message);
                        break;
                    }

                    // Форматируем данные.
                    List<string> result = new List<string>();
                    foreach (string str in data)
                    {
                        try
                        {
                            result.Add(Utility.Format(str));
                        }
                        catch (FormatException ex)
                        {
                            result.Add(ex.Message);
                        }
                    }

                    // Если аргумент один (только исходный файл), пишем в него.
                    if (args.Length == 1)
                    {
                        try
                        {
                            Utility.WriteFile(sourceFileName, result);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("The file could not be written:");
                            Console.WriteLine(e.Message);
                            break;
                        }
                    }

                    // Если аргумента два (исходный и целевой файлы), то пишем в целевой.
                    if (args.Length == 2)
                    {
                        string targetFileName = args[1];
                        try
                        {
                            Utility.WriteFile(targetFileName, result);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("The file could not be written:");
                            Console.WriteLine(e.Message);
                            break;
                        }
                    }

                    break;

                default:

                    Console.WriteLine("Invalid arguments count.");
                    break;
            }
        }
    }
}
