﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1_Utility
{
    /// <summary>
    /// Предоставляет реализации общих методов.
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// Пример данных для форматирования.
        /// </summary>
        public static string[] EXAMPLE_DATA = { "23.8976,12.3218", "25.7639,11.9463", "24.8293,12.2134" };

        /// <summary>
        /// Форматирует входные данные.
        /// </summary>
        /// <param name="data">Строку, представляющие собой данные для форматирования.</param>
        /// <returns>Строку, представляющие собой результат форматирования.</returns>
        public static string Format(string data)
        {
            
            string[] substrs = data.Split(new char[] { ',' }, 2);
            // Длинная версия метода Double.TryParse используется только ради подстановки CultureInfo.InvariantCulture с целью заставить компилятор видеть точки как отделители дробной части, в противном случае ищет запятые.
            if (!Double.TryParse(substrs[0], NumberStyles.Any, CultureInfo.InvariantCulture, out double x) || !Double.TryParse(substrs[1], NumberStyles.Any, CultureInfo.InvariantCulture, out double y))
            {
                throw new FormatException("The following data cannot be parsed from string to double: " + data);         
            }

            return "X: " + x.ToString("0.0000") + "    Y: " + y.ToString("0.0000");
        }

        /// <summary>
        /// Читает файл по строкам.
        /// </summary>
        /// <param name="fileName">Имя файла.</param>
        /// <returns>Список прочитанных строк.</returns>
        public static List<string> ReadFile(string fileName)
        {
            List<string> result = new List<string>();
            // Стырено с MSDN: https://docs.microsoft.com/ru-ru/dotnet/standard/io/how-to-read-text-from-a-file
            // Open the text file using a stream reader.
            using (StreamReader sr = new StreamReader(fileName))
            {
                // Читаем строки, пока они есть, и добавляем их в List.
                while (!sr.EndOfStream)
                {
                    result.Add(sr.ReadLine());
                }
            }
            return result;
        }

        /// <summary>
        /// Пишет в файл по строкам.
        /// </summary>
        /// <param name="fileName">Имя файла.</param>
        /// <param name="data">Список строк для записи.</param>
        public static void WriteFile(string fileName, List<string> data)
        {
            // Стырено с MSDN: https://docs.microsoft.com/ru-ru/dotnet/standard/io/how-to-write-text-to-a-file
            using (StreamWriter outputFile = new StreamWriter(fileName))
            {
                foreach (string line in data)
                    outputFile.WriteLine(line);
            }
        }
    }
}
