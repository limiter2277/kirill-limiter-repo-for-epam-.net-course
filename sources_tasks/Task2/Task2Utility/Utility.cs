﻿using System;

namespace Task2.Utility
{
    /// <summary>
    /// Класс, содержащий логику для задания 2 (нахождение корня из числа методом Ньютона).
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// Вычисляет корень заданной степени из числа с указанной точностью методом Ньютона.
        /// </summary>
        /// <param name="number">Число.</param>
        /// <param name="degree">Степень корня.</param>
        /// <param name="precision">Точность.</param>
        /// <returns>Корень заданной степени из числа.</returns>
        public static double CalculateRoot(double number, int degree, double precision)
        {
            double degr = degree; // Далее используем double-версию параметра degree, потому что Л - логика, и 1/degree это int, а не double, и x1 тут же обращается в нуль.
            double x0 = number / degr; // Начальное предположение, пусть будет число/степень, получится не слишком далеко от истины (наверное).
            double x1 = 1 / degr * ((degr - 1) * x0 + number / Math.Pow(x0, degr - 1));
            while(Math.Abs(x1 - x0) > precision)
            {
                x0 = x1;
                x1 = 1 / degr * ((degr - 1) * x0 + number / Math.Pow(x0, degr - 1));
            }

            return x1;
        }
    }
}
