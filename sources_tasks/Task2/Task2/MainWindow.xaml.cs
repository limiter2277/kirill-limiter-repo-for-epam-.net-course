﻿using System;
using System.Windows;
using System.Globalization;

namespace Task2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LogTextBox.Text += "Программа запущена." + Environment.NewLine;
        }

        private void CalculateRootButton_Click(object sender, RoutedEventArgs e)
        {
            double number, precision;
            int degree;

            byte i = 0; // Для определения ошибочного поля в try/catch.
            string[] fields = { "unknown", NumberTextBlock.Text, DegreeTextBlock.Text, PrecisionTextBlock.Text };
            try
            {
                i = 1;
                number = Convert.ToDouble(NumberTextBox.Text, CultureInfo.InvariantCulture);
                i = 2;
                degree = Convert.ToInt32(DegreeTextBox.Text);
                i = 3;
                precision = Convert.ToDouble(PrecisionTextBox.Text, CultureInfo.InvariantCulture);

                LogTextBox.Text += $"Корень числа {number} в степени {degree} с точностью {precision} равен {Task2.Utility.Utility.CalculateRoot(number, degree, precision).ToString("0.0000")}. Результат от Math.Pow: {Math.Pow(number, (double)1/degree).ToString("0.0000")}." + Environment.NewLine;
            }
            catch (Exception ex)
            {
                LogTextBox.Text += $"Невозможно выполнить конвертацию поля {fields[i]}: {ex.Message}" + Environment.NewLine;
            }
            LogTextBox.ScrollToEnd();
        }

        private void ConvertButton_Click(object sender, RoutedEventArgs e)
        {
            int number = 0;
            try
            {
                number = Convert.ToInt32(NumberToConvertTextBox.Text);
                if (number < 0)
                {
                    throw new Exception("Введенное число не может быть отрицательным.");
                }
                LogTextBox.Text += $"Число {number} в двоичной форме: {Convert.ToString(number, 2)}." + Environment.NewLine;
            }
            catch(Exception ex)
            {
                LogTextBox.Text += $"Невозможно выполнить конвертацию: {ex.Message}" + Environment.NewLine;
            }
            LogTextBox.ScrollToEnd();
        }
    }
}
