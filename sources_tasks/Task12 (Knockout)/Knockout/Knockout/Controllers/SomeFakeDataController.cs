﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Knockout.Models;

namespace Knockout.Controllers
{
    public class SomeFakeDataController : ApiController
    {
		public IEnumerable<SomeFakeData> Get()
		{
			return new List<SomeFakeData>
			{
				new SomeFakeData { Id = 1, Title = "Title1", DateCreated = DateTime.Now.AddDays(-1) },
				new SomeFakeData { Id = 2, Title = "Title2", DateCreated = DateTime.Now },
				new SomeFakeData { Id = 3, Title = "Title3", DateCreated = DateTime.Now.AddDays(-11) }
			};
		}
    }
}
