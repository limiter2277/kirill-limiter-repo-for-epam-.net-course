﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knockout.Models
{
	public class SomeFakeData
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public DateTime DateCreated { get; set; }
	}
}