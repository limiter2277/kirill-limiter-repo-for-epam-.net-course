﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task7.Factories;
using Task7.Products;

namespace Task7
{
    class Client
    {
        public IPizzaMargaretta MargarettaPizza { get; protected set; }
        public IPizzaPepperoni PepperoniPizza { get; protected set; }

        public Client(IFactory factory)
        {
            MargarettaPizza = factory.MakePizzaMargaretta();
            PepperoniPizza = factory.MakePizzaPepperoni();
        }
    }
}
