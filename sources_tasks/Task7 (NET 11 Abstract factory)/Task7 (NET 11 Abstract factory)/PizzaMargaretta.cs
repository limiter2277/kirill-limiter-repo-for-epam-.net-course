﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7.Products
{
    class PizzaMargaretta : IPizzaMargaretta
    {
        public string Margaretta { get; set; }

        public PizzaMargaretta(string m)
        {
            Margaretta = m;
        }

        public override string ToString()
        {
            return Margaretta;
        }
    }
}
