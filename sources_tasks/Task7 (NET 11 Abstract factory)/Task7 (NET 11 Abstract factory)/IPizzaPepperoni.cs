﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7.Products
{
    interface IPizzaPepperoni
    {
        string Pepperoni { get; set; }
    }
}
