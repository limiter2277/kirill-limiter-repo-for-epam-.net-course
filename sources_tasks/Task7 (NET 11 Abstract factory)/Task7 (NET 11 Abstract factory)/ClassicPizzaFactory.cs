﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task7.Products;

namespace Task7.Factories
{
    class ClassicPizzaFactory : IFactory
    {
        public IPizzaMargaretta MakePizzaMargaretta()
        {
            return new PizzaMargaretta("ClassicMargaretta");
        }

        public IPizzaPepperoni MakePizzaPepperoni()
        {
            return new PizzaPepperoni("ClassicPepperoni");
        }
    }
}
