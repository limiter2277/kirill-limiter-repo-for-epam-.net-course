﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task7.Products;

namespace Task7.Factories
{
    interface IFactory
    {
        IPizzaMargaretta MakePizzaMargaretta();
        IPizzaPepperoni MakePizzaPepperoni();
    }
}
