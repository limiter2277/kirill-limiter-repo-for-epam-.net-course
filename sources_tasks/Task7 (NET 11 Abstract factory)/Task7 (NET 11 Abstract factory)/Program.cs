﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task7.Factories;

namespace Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            var classicClient = new Client(new ClassicPizzaFactory());
            var thinClient = new Client(new ThinPizzaFactory());

            Console.WriteLine($"Classic client wants {classicClient.MargarettaPizza} and {classicClient.PepperoniPizza}.");
            Console.WriteLine($"Thin client wants {thinClient.MargarettaPizza} and {thinClient.PepperoniPizza}.");
            Console.ReadKey();
        }
    }
}
