﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7.Products
{
    class PizzaPepperoni : IPizzaPepperoni
    {
        public string Pepperoni { get; set; }

        public PizzaPepperoni(string p)
        {
            Pepperoni = p;
        }

        public override string ToString()
        {
            return Pepperoni;
        }
    }
}
