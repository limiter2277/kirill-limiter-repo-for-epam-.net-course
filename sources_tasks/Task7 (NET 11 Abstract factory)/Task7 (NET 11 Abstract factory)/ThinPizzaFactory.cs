﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task7.Products;

namespace Task7.Factories
{
    class ThinPizzaFactory : IFactory
    {
        public IPizzaMargaretta MakePizzaMargaretta()
        {
            return new PizzaMargaretta("ThinMargaretta");
        }

        public IPizzaPepperoni MakePizzaPepperoni()
        {
            return new PizzaPepperoni("ThinPepperoni");
        }
    }
}
