import { Component, OnInit } from '@angular/core';
import { SomeFakeData } from '../some-fake-data';
import { SomeFakeDataService } from '../some-fake-data.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {
  data: SomeFakeData[];

  GetData(): void {
    this.someFakeDataService.GetData().subscribe((dataReturned:SomeFakeData[]) => this.data = dataReturned);
  }

  constructor(private someFakeDataService: SomeFakeDataService) { }

  ngOnInit() {
    this.data = [];
  }

}
