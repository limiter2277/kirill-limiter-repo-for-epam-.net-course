import { TestBed, inject } from '@angular/core/testing';

import { SomeFakeDataService } from './some-fake-data.service';

describe('SomeFakeDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SomeFakeDataService]
    });
  });

  it('should be created', inject([SomeFakeDataService], (service: SomeFakeDataService) => {
    expect(service).toBeTruthy();
  }));
});
