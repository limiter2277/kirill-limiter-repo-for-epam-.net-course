import { Injectable } from '@angular/core';
import { SomeFakeData } from './some-fake-data';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SomeFakeDataService {
  private webApiUrl = 'http://localhost:4242/api/SomeFakeData';

  GetData(): Observable<SomeFakeData[]> {
    return this.http.get<SomeFakeData[]>(this.webApiUrl);
  }

  constructor(private http: HttpClient) { }
}