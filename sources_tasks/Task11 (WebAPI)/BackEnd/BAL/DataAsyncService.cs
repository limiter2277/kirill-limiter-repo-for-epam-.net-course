﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace BAL
{
    public class DataAsyncService : IDataAsyncService
    {
        private IDataAsyncRepository _dataRepository;

        public DataAsyncService(IDataAsyncRepository repository)
        {
            _dataRepository = repository;
        }

        public async Task<IEnumerable<SomeFakeData>> GetDataAsync()
        {
            return await _dataRepository.ReadAsync();
        }
    }
}
