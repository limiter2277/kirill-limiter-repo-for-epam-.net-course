﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;

namespace DAL
{
    public class SomeFakeDataAsyncRepository : IDataAsyncRepository
    {
        public async Task<IEnumerable<SomeFakeData>> ReadAsync()
        {
            return await Task.Run(() =>
            {
                return new List<SomeFakeData>
                    {
                        new SomeFakeData { Id = 1, Title = "Title1", DateCreated = DateTime.Now.AddDays(-1) },
                        new SomeFakeData { Id = 2, Title = "Title2", DateCreated = DateTime.Now },
                        new SomeFakeData { Id = 3, Title = "Title3", DateCreated = DateTime.Now.AddDays(-11) }
                    };
            });
        }
    }
}
