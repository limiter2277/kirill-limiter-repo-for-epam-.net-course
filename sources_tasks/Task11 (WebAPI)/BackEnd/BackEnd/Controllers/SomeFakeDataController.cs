﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Common;

namespace BackEnd.Controllers
{
    public class SomeFakeDataController : ApiController
    {
        private IDataAsyncService _dataService;

        public SomeFakeDataController(IDataAsyncService dataService)
        {
            _dataService = dataService;
        }

        // GET api/<controller>
        public Task<IEnumerable<SomeFakeData>> Get()
        {
            return _dataService.GetDataAsync();
        }

        // GET api/<controller>/5
        public SomeFakeData Get(int id)
        {
            return null;
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}