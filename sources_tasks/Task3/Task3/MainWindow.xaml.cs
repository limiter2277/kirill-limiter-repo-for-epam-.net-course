﻿using System;
using System.Windows;

namespace Task3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Task3.MailToData.MailToData data = new MailToData.MailToData();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SaveDataButton_Click(object sender, RoutedEventArgs e)
        {
            data.Name = NameTextBox.Text.Trim();

            try
            {
                data.Address = AddressTextBox.Text.Trim();
                PrintDataTextBox.Text = "Имя: " + (data.Name == String.Empty ? "имя не указано" : data.Name) + ". Адрес: " + data.Address;
            }
            catch
            {
                PrintDataTextBox.Text = "Не удалось прочитать почтовый индекс. Он должен идти в качестве третьего параметра через запятую и быть положительным целым числом.";
            }

            PrintDataTextBox.ScrollToEnd();
        }
    }
}
