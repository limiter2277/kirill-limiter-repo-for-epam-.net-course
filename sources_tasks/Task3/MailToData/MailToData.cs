﻿using System;

namespace Task3.MailToData
{
    /// <summary>
    /// Класс, хранящий в себе имя и адрес человека. Адрес представлен в виде города, области и почтового индекса.
    /// </summary>
    public class MailToData
    {
        private string city = String.Empty;
        private string state = String.Empty;
        private int zip = 0;

        /// <summary>
        /// Имя человека.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Адрес человека (город, область, почтовый индекс).
        /// </summary>
        public string Address
        {
            get
            {
                return (city == String.Empty ? "город не указан" : city) + ", " + (state == String.Empty ? "область не указана" : state) + ", " + (zip == 0 ? "индекс не указан" : zip.ToString()) + ".";
            }
            set
            {
                string[] substrs = value.Split(new char[] { ',' }, 3);
                city = substrs.Length >= 1 ? substrs[0].Trim() : String.Empty;
                state = substrs.Length >= 2 ? substrs[1].Trim() : String.Empty;
                try
                {
                    zip = substrs.Length >= 3 ? Convert.ToInt32(substrs[2]) : 0;
                }
                catch
                {
                    zip = 0;
                    throw new Exception("Неправильный индекс.");
                }
                if (zip <= 0)
                {
                    zip = 0;
                    throw new Exception("Неправильный индекс.");
                }
            }
        }
    }
}
