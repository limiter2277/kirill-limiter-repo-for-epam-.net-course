﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    /// <summary>
    /// Интерфейс для операций конвертации кода в определенный язык программирования.
    /// </summary>
    interface IConvertible
    {
        /// <summary>
        /// Конвертирует код в язык C#.
        /// </summary>
        /// <param name="code">Код для конвертации.</param>
        /// <returns>Код на языке C#.</returns>
        string ConvertToCSharp(string code);

        /// <summary>
        /// Конвертирует код в язык VB.
        /// </summary>
        /// <param name="code">Код для конвертации.</param>
        /// <returns>Код на языке VB.</returns>
        string ConvertToVB(string code);
    }
}
