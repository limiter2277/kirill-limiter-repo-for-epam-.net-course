﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            // Считаем, что текст, написанный строчными буквами - это VB, а прописными - C#.

            string exampleString = "Some random TEXT HERE. HELLO worLD.";

            var list = new List<ProgramConverter>
            {
                new ProgramConverter(),
                new ProgramConverter(),
                new ProgramHelper(),
                new ProgramConverter(),
                new ProgramHelper()
            };

            Console.WriteLine("Example string: " + exampleString);
            foreach (var item in list)
            {
                Console.WriteLine("item is " + item.GetType());
                try
                {
                    var code = exampleString;
                    if (item is ICodeChecker)
                    {
                        Console.WriteLine("Is C# code? " + (item as ICodeChecker).CheckCodeSyntax(code, "C#"));
                        code = item.ConvertToCSharp(code);
                        Console.WriteLine("Converted to CSharp: " + code);
                        Console.WriteLine("Is C# code? " + (item as ICodeChecker).CheckCodeSyntax(code, "C#"));
                    }
                    else
                    {
                        code = item.ConvertToCSharp(code);
                        Console.WriteLine("Converted to CSharp: " + code);
                        code = item.ConvertToVB(code);
                        Console.WriteLine("Converted to VB: " + code);
                    }
                    Console.WriteLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            Console.ReadLine();
        }
    }
}
