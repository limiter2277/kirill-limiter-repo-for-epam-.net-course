﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    /// <summary>
    /// Класс для помощи в работе с программным кодом.
    /// </summary>
    class ProgramHelper : ProgramConverter, ICodeChecker
    {
        /// <summary>
        /// Проверяет, соответствует ли код указанному языку программирования.
        /// </summary>
        /// <param name="code">Код для проверки.</param>
        /// <param name="lang">Язык программирования.</param>
        /// <returns>Результат проверки: да или нет.</returns>
        public bool CheckCodeSyntax(string code, string lang)
        {
            if (lang != "C#" && lang != "VB")
            {
                throw new ArgumentException("Аргумент lang задан неправильно. Допустимые значения: \"C#\" или \"VB\".");
            }
            if (lang == "C#" && String.Compare(code, code.ToUpperInvariant(), false) == 0)
            {
                return true;
            }
            if (lang == "VB" && String.Compare(code, code.ToLowerInvariant(), false) == 0)
            {
                return true;
            }
            return false;
        }
    }
}
