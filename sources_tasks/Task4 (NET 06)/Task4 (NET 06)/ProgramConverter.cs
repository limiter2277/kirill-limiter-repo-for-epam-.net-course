﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    /// <summary>
    /// Класс для конвертации кода в разные языки программирования.
    /// </summary>
    class ProgramConverter : IConvertible
    {
        /// <summary>
        /// Конвертирует код в язык C#.
        /// </summary>
        /// <param name="code">Код для конвертации.</param>
        /// <returns>Код на языке C#.</returns>
        public string ConvertToCSharp(string code)
        {
            return code.ToUpperInvariant();
        }

        /// <summary>
        /// Конвертирует код в язык VB.
        /// </summary>
        /// <param name="code">Код для конвертации.</param>
        /// <returns>Код на языке VB.</returns>
        public string ConvertToVB(string code)
        {
            return code.ToLowerInvariant();
        }
    }
}
