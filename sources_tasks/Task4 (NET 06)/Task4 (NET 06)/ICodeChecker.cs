﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    /// <summary>
    /// Интерфейс для операций проверки кода.
    /// </summary>
    interface ICodeChecker
    {
        /// <summary>
        /// Проверяет, соответствует ли код указанному языку программирования.
        /// </summary>
        /// <param name="code">Код для проверки.</param>
        /// <param name="lang">Язык программирования.</param>
        /// <returns>Результат проверки: да или нет.</returns>
        bool CheckCodeSyntax(string code, string lang);
    }
}
