﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task9
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] sides1 = { 2, 3, 4 };
            int[] sides2 = { 2, 3, 6 };
            Triangle t1 = null, t2 = null;

            Console.WriteLine($"Пробуем создать треугольники со сторонами t1({sides1[0]} {sides1[1]} {sides1[2]}) и t2({sides2[0]} {sides2[1]} {sides2[2]}).");
            try
            {
                t1 = new Triangle(sides1[0], sides1[1], sides1[2]);
                t2 = new Triangle(sides2[0], sides2[1], sides2[2]);
            }
            catch { }

            Console.WriteLine($"t1: {((t1 is Triangle) ? "успешно" : "неуспешно")}, t2: {((t2 is Triangle) ? "успешно" : "неуспешно")}.");

            Console.WriteLine($"Периметр t1: {Triangle.Perimeter(t1)}. Площадь t1: {Triangle.Square(t1)}.");

            Console.ReadKey();
        }
    }
}
