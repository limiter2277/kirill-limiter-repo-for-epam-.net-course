﻿using System;

namespace Task9
{
    public class Triangle
    {
        /// <summary>
        /// Одна из сторон треугольника.
        /// </summary>
        public double A { get; protected set; }

        /// <summary>
        /// Одна из сторон треугольника.
        /// </summary>
        public double B { get; protected set; }

        /// <summary>
        /// Одна из сторон треугольника.
        /// </summary>
        public double C { get; protected set; }

        public Triangle(double a, double b, double c)
        {
            if (IsValid(a, b, c))
            {
                A = a; B = b; C = c;
            }
            else
            {
                throw new ArgumentException("Треугольник с такими сторонами существовать не может.");
            }
        }

        /// <summary>
        /// Проверяет, может ли существовать треугольник с указанными сторонами.
        /// </summary>
        /// <param name="a">Одна из сторон треугольника.</param>
        /// <param name="b">Одна из сторон треугольника.</param>
        /// <param name="c">Одна из сторон треугольника.</param>
        /// <returns>Результат проверки.</returns>
        public static bool IsValid(double a, double b, double c)
        {
            return (a < b + c) && (b < a + c) && (c < a + b);
        }

        /// <summary>
        /// Возвращает периметр указанного треугольника.
        /// </summary>
        /// <param name="t">Треугольник.</param>
        /// <returns>Периметр треугольника.</returns>
        public static double Perimeter(Triangle t)
        {
            return t.A + t.B + t.C;
        }

        /// <summary>
        /// Возвращает площадь указанного треугольника, расчитанную по формуле Герона.
        /// </summary>
        /// <param name="t">Треугольник.</param>
        /// <returns>Площадь треугольника.</returns>
        public static double Square(Triangle t)
        {
            double p = Perimeter(t) / 2;
            return Math.Pow(p * (p - t.A) * (p - t.B) * (p - t.C), 0.5);
        }
    }
}
