﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task9;

namespace TriangleTest
{
    [TestClass]
    public class TriangleTest
    {
        [TestMethod]
        public void TriangleSidesIsValid_ValidSides_2_3_4_ResultTrue()
        {
            // Arrange
            var expected = true;

            // Act
            var actual = Triangle.IsValid(2, 3, 4);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TriangleCreation_ValidSides_2_3_4_Success()
        {
            var t = new Triangle(2, 3, 4);
        }

        [TestMethod]
        public void TriangleCreation_InvalidSides_2_3_6_Fail()
        {
            Assert.ThrowsException<ArgumentException>(() =>
            {
                var t = new Triangle(2, 3, 6);
            });
        }

        [TestMethod]
        public void TrianglePerimeter_TriangleSides_2_3_4_Result_9()
        {
            // Arrange
            var t = new Triangle(2, 3, 4);
            var expected = 9;

            // Act
            var actual = Triangle.Perimeter(t);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TriangleSquare_TriangleSides_2_3_4_Result_2point9()
        {
            // Arrange
            var t = new Triangle(2, 3, 4);
            double expected = 2.9;

            // Act
            double actual = Triangle.Square(t);

            // Assert
            Assert.AreEqual(expected, actual, 0.01);
        }
    }
}
