﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.IO;

namespace Task8
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Title = $"{WINDOW_NAME} Текущий документ: {(_currentFilePath == String.Empty ? "новый документ" : Path.GetFileName(_currentFilePath))}";
        }

        const string WINDOW_NAME = "Task8 Text Editor";
        string _currentFilePath = String.Empty;

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            _currentFilePath = String.Empty;
            EditorTextBox.Text = String.Empty;
            Title = $"{WINDOW_NAME} Текущий документ: {(_currentFilePath == String.Empty ? "новый документ" : Path.GetFileName(_currentFilePath))}";
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            // Спижжено с MSDN: https://msdn.microsoft.com/ru-ru/library/microsoft.win32.openfiledialog(v=vs.110).aspx
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Текстовый документ"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Текстовые документы (.txt)|*.txt"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                _currentFilePath = dlg.FileName;

                try
                {
                    EditorTextBox.Text = File.ReadAllText(Path.GetFullPath(_currentFilePath));
                }
                catch (Exception ex)
                {
                    _currentFilePath = String.Empty;
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            Title = $"{WINDOW_NAME} Текущий документ: {(_currentFilePath == String.Empty ? "новый документ" : Path.GetFileName(_currentFilePath))}";
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (_currentFilePath != String.Empty)
            {
                Save();
            }
            else
            {
                SaveAs();
                Save();
            }
        }

        private void SaveAsButton_Click(object sender, RoutedEventArgs e)
        {
            SaveAs();
            Save();
        }

        private void Save()
        {
            try
            {
                File.WriteAllText(Path.GetFullPath(_currentFilePath), EditorTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveAs()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Текстовый документ"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Текстовые документы (.txt)|*.txt"; // Filter files by extension

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                _currentFilePath = dlg.FileName;
            }

            Title = $"{WINDOW_NAME} Текущий документ: {(_currentFilePath == String.Empty ? "новый документ" : Path.GetFileName(_currentFilePath))}";
        }
    }
}
