﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Common;
using DAL;

namespace BackEnd.Controllers
{
    public class UserTypeController : ApiController
    {
        private IDataService<UserType> _dataService;

        public UserTypeController(IDataService<UserType> dataService)
        {
            _dataService = dataService;
        }

        // GET api/<controller>
        public IEnumerable<UserType> Get()
        {
            return _dataService.GetData();
        }

        // GET api/<controller>/5
        public UserType Get(int id)
        {
            return null;
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}