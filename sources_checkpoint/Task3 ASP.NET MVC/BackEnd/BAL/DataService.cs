﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BAL
{
    public class DataService<T> : IDataService<T>
    {
        private IDataRepository<T> _dataRepository;

        public DataService(IDataRepository<T> repository)
        {
            _dataRepository = repository;
        }

        public IEnumerable<T> GetData()
        {
            return _dataRepository.Read();
        }

		public void UpdateData(IEnumerable<T> data)
		{
			_dataRepository.Update(data);
		}
	}
}
