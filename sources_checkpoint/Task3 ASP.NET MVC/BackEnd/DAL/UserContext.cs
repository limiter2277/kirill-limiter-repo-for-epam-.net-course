﻿using Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
	public class UserContext : DbContext
	{
		public UserContext() : base("DBConnection")
		{
			Database.SetInitializer<UserContext>(new UserContextInitializer());
		}

		public DbSet<UserData> UserData { get; set; }
		public DbSet<UserType> UserType { get; set; }
	}
}
