﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
	[Table("UserData")]
	public class UserData
    {
		[Key]
        public int Id { get; set; }

        public string LastName { get; set; }

		[Required]
		public string FirstName { get; set; }

		public string MiddleName { get; set; }

		public string Phone { get; set; }

		public string Email { get; set; }

		public int? TypeId { get; set; }

		public UserType Type { get; set; }
	}
}
