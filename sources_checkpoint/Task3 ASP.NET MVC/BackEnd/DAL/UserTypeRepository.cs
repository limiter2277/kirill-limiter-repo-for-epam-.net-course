﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;

namespace DAL
{
    public class UserTypeRepository : IDataRepository<UserType>
    {
		UserContext _context;

		public UserTypeRepository(UserContext context)
		{
			_context = context;
		}

        public IEnumerable<UserType> Read()
        {
			return _context.UserType.ToList();
		}

		public void Update(IEnumerable<UserType> data)
		{
			throw new NotImplementedException();
		}
	}
}
