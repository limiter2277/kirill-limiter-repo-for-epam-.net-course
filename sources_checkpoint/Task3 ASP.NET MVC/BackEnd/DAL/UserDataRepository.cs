﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;

namespace DAL
{
    public class UserDataRepository : IDataRepository<UserData>
    {
		UserContext _context;

		public UserDataRepository(UserContext context)
		{
			_context = context;
		}

        public IEnumerable<UserData> Read()
        {
			return _context.UserData.ToList();
		}

		public void Update(IEnumerable<UserData> data)
		{
			var list = new List<UserData>();
			foreach (var item in data)
			{
				var entity =_context.UserData.Find(item.Id);
				_context.Entry(entity).CurrentValues.SetValues(item);
				_context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
			}
			_context.SaveChanges();
		}
    }
}
