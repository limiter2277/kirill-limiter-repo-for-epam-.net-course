﻿using Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
	class UserContextInitializer : /*DropCreateDatabaseAlways<UserContext>*/ CreateDatabaseIfNotExists<UserContext>
	{
		protected override void Seed(UserContext db)
		{
			// User types
			db.UserType.Add(new UserType
			{
				Name = "Common user"
			});
			db.UserType.Add(new UserType
			{
				Name = "Superhero"
			});
			db.SaveChanges();

			// Users
			db.UserData.Add(new UserData {
				LastName = "Картошкин",
				FirstName = "Валентин",
				MiddleName = "Петрович",
				Phone = "89084576356",
				Email = "kartoffkin@mail.ru"
				// TypeId is null
			});
			db.UserData.Add(new UserData
			{
				LastName = "Шляпкин",
				FirstName = "Николай",
				MiddleName = "Александрович",
				Phone = "89345653446",
				Email = "nick@mail.ru",
				TypeId = 1
			});
			db.UserData.Add(new UserData
			{
				LastName = "Белкина",
				FirstName = "Мария",
				MiddleName = "Васильевна",
				Phone = "89084556856",
				Email = "mariabelka@mail.ru",
				TypeId = 2
			});

			db.SaveChanges();
		}
	}
}
