import { Component, OnInit } from '@angular/core';
import { UserData } from '../user-data';
import { UserType } from '../user-type';
import { UserDataService } from '../user-data.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {
  data: UserData[];
  types: UserType[];

  GetData(): void {
    this.userDataService.GetData().subscribe((dataReturned:UserData[]) => this.data = dataReturned);
    //this.userDataService.GetUserTypes().subscribe((dataReturned:UserType[]) => this.types = dataReturned);
  }

  SaveData(): void {
    this.userDataService.UpdateData(this.data);
  }

  constructor(private userDataService: UserDataService) { }

  ngOnInit() {
    this.data = [];
    this.types = [];
  }

}
