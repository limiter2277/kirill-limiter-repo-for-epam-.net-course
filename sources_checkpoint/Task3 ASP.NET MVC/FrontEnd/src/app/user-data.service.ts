import { Injectable } from '@angular/core';
import { UserData } from './user-data';
import { UserType } from './user-type';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  private webApiUserDataUrl = 'http://localhost:4242/api/UserData';
  private webApiUserTypeUrl = 'http://localhost:4242/api/UserType';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  GetData(): Observable<UserData[]> {
    return this.http.get<UserData[]>(this.webApiUserDataUrl);
  }

  GetUserTypes(): Observable<UserType[]> {
    return this.http.get<UserType[]>(this.webApiUserTypeUrl);
  }

  UpdateData (data: UserData[]): Observable<any> {
    return this.http.put(this.webApiUserDataUrl, data, this.httpOptions);
  }

  constructor(private http: HttpClient) { }
}