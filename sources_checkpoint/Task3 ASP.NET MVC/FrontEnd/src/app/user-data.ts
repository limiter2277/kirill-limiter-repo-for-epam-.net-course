export class UserData {
    Id: number;
    LastName: string;
    FirstName: string;
    MiddleName: string;
    Phone: string;
    Email: string;
    TypeId: number;
}
