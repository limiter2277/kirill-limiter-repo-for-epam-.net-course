﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1_Extension
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new List<string>();
            list.Add("Hello world!");
            list.Add("Hello space!");
            list.Add("Greetings");
            list.Add("You are not prepared");
            list.Add("Than I said hello");
            list.Add("This string is made to create the third item in result lol. Hello!");

            foreach (var str in list.FindStringContains("Hello"))
            {
                Console.WriteLine(str);
            }

            Console.ReadKey();
        }
    }
}
