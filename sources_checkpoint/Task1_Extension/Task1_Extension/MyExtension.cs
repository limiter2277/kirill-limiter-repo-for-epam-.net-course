﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1_Extension
{
    static class MyExtension
    {
        public static IEnumerable<string> FindStringContains(this IEnumerable<string> collection, string input)
        {
            /*
            var result = new List<string>();
            foreach (var str in collection)
            {
                if (str.Contains(input))
                {
                    result.Add(str);
                }
            }
            */
            var result = from str in collection where str.Contains(input) select str;

            return result;
        }
    }
}
