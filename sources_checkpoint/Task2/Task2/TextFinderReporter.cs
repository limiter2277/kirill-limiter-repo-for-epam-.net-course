﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class TextFinderReporter
    {
        public List<FileInfo> Files { get; protected set; }
        public string TargetString { get; protected set; }

        public TextFinderReporter(IEnumerable<string> files, string targetString)
        {
            TargetString = targetString;
            Files = new List<FileInfo>();
            foreach (var file in files)
            {
                Files.Add(new FileInfo(file));
            }
        }

        struct DetailedInfo
        {
            public int Count;
            public string FileName;
            public string FileDir;
            public long FileSize;
            public DateTime FileLastAccessTime;
        }
        int DetailedInfoReverseCompare(DetailedInfo x, DetailedInfo y) => x.Count.CompareTo(y.Count) * (-1);

        public string GetReport()
        {
            var report = new StringBuilder();
            report.AppendLine($"The following list is the files contain \"{TargetString}\" string.");

            var filesDetailedInfo = new List<DetailedInfo>();
            foreach (var file in Files)
            {
                DetailedInfo d;
                d.Count = File.ReadAllText(file.FullName).Split(new string[] { TargetString }, StringSplitOptions.None).Count() - 1;
                d.FileName = file.Name;
                d.FileDir = file.Directory.ToString();
                d.FileSize = file.Length;
                d.FileLastAccessTime = file.LastAccessTime;
                filesDetailedInfo.Add(d);
            }
            filesDetailedInfo.Sort(DetailedInfoReverseCompare);
            foreach (var d in filesDetailedInfo)
            {
                report.AppendLine($"Count: {d.Count}. File name: {d.FileName}. File directory: {d.FileDir}. File size: {d.FileSize} bytes. Last access time: {d.FileLastAccessTime}.");
            }
            return report.ToString();
        }
    }
}
