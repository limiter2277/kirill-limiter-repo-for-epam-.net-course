﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    static class TextFinder
    {
        public static IEnumerable<string> FindFilesContainsString(DirectoryInfo directory, string targetString)
        {
            var files = new List<string>(Directory.EnumerateFiles(directory.FullName));
            foreach (var dir in Directory.EnumerateDirectories(directory.FullName))
            {
                files.AddRange(Directory.EnumerateFiles(dir));
            }
            foreach (var r in (from file in files where File.ReadAllText(file).Contains(targetString) select file))
            {
                yield return r;
            }
        }
    }
}
