﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            // You can find example textfiles folder in the solution folder.
            DirectoryInfo dir = new DirectoryInfo(@"C:\Users\net\Desktop\textfiles");

            if (args.Length > 0)
            {
                dir = new DirectoryInfo(args[0]);
            }

            try
            {
                Console.WriteLine(new TextFinderReporter(TextFinder.FindFilesContainsString(dir, "Cheers"), "Cheers").GetReport());
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
